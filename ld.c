/*------------------------------------------------------------------------+
| NOMBRE:Ricardo Fernando Morf�n Ch�vez             MATR�CULA:347553      |
|                                                                         |
| FECHA: 29/09/17                                                         |
|                                                                         |
| DECRIPCI�N:                                                             |
| 1. Agregar                        				        			  |
| 2. Eliminar                                                             |
| 3. Buscar                            				        			  |
| 4. Imprimir                       				        			  |
| 5. Salir                          				        			  |
+------------------------------------------------------------------------*/

/*------------------------------------------------------------------------+
| LIBRERIAS                                                               |
+------------------------------------------------------------------------*/

#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

#include "valida.h"

/*------------------------------------------------------------------------+
| DEFINICI�N TIPOS DE DATOS                                               |
+------------------------------------------------------------------------*/

typedef struct _tDatoD {
    struct _tDatoD *ant;
    int     dato;
    struct _tDatoD *sig;
}tDatoD;

typedef tDatoD *tNodoD;

/*------------------------------------------------------------------------+
| CONSTANTES                                                              |
+------------------------------------------------------------------------*/

/*------------------------------------------------------------------------+
| PROTOTIPOS DE FUNCIONES                                                 |
+------------------------------------------------------------------------*/

// Colas
tNodoD genDatosD(void);
void AgregarLD(tNodoD *listaD, tNodoD *ultimo, tNodoD *nuevo);
int EliminarLD(tNodoD *listaD, tNodoD *ultimo, tNodoD *nodo);
void ImprimirLD(tNodoD listaD);
void ServicioND(tNodoD temp);
tNodoD BuscarLD(tNodoD listaD, tNodoD ultimo, int valor);

// Men�
void menu(void);

/*------------------------------------------------------------------------+
| FUNCI�N MAIN 			                                                  |
+------------------------------------------------------------------------*/

int main(void)
{
    srand(time(NULL));
   	menu();

   	return 0;
}

/*------------------------------------------------------------------------+
| FUNCION PARA GENERAR MEMORIA Y LEER DATOS                               |
+------------------------------------------------------------------------*/

tNodoD genDatosD(void)
{
    tNodoD temp = NULL;
    temp = (tNodoD)malloc(sizeof(tDatoD));
    temp->ant = temp->sig = NULL;
    temp->dato = rand()%3;

    return temp;
}

/*------------------------------------------------------------------------+
| Agregar numero en lista                                                 |
+------------------------------------------------------------------------*/

void AgregarLD(tNodoD *listaD, tNodoD *ultimo, tNodoD *nuevo)
{
    tNodoD  temp = NULL;

    if(!*listaD)
    {
        *listaD = *ultimo = *nuevo;
    }
    else if((*nuevo)->dato <= (*listaD)->dato)
    {
        if((*nuevo)->dato != (*listaD)->dato)
        {
            (*nuevo)->sig = *listaD;
            (*listaD)->ant = *nuevo;
            *listaD = *nuevo;
        }
        else
        {
            free(*nuevo);
        }
    }
    else if((*nuevo)->dato >= (*ultimo)->dato)
    {
        if((*nuevo)->dato != (*ultimo)->dato)
        {
            (*ultimo)->sig = *nuevo;
            (*nuevo)->ant = *ultimo;
            *ultimo = *nuevo;
        }
        else
        {
            free(*nuevo);
        }
    }
    else
    {
        temp = *listaD;

        while(temp->sig && (temp->sig)->dato <= (*nuevo)->dato)
            temp = temp->sig;

        if(temp->dato == (*nuevo)->dato)
        {
            free(*nuevo);
        }
        else
        {
            (*nuevo)->ant = temp;
            (*nuevo)->sig = temp->sig;
            (temp->sig)->ant = *nuevo;
            temp->sig = *nuevo;
        }
    }
    *nuevo = NULL;
}

/*------------------------------------------------------------------------+
| Eliminar                                                                |
+------------------------------------------------------------------------*/

int EliminarLD(tNodoD *listaD, tNodoD *ultimo, tNodoD *nodo)
{
    if(*nodo)
    {
        if(*listaD == *nodo)
        {
            if(*listaD == *ultimo)
                *listaD = *ultimo = NULL;
            else
            {
                ((*nodo)->sig)->ant = NULL;
                *listaD = ((*nodo)->sig);
            }
            free(*nodo);
            *nodo = NULL;
            return 0;
        }
        else if(*ultimo == *nodo)
        {
            ((*nodo)->ant)->sig = NULL;
            *ultimo = ((*nodo)->ant);
            free(*nodo);
            *nodo = NULL;
            return 0;
        }
        else
        {
            ((*nodo)->sig)->ant = (*nodo)->ant;
            ((*nodo)->ant)->sig = (*nodo)->sig;
            free(*nodo);
            *nodo = NULL;
            return 0;
        }
    }
    else
        return -1;
}


/*------------------------------------------------------------------------+
| Imprimir                                                                |
+------------------------------------------------------------------------*/

void ImprimirLD(tNodoD listaD)
{
    tNodoD temp;

    temp = listaD;
    while(temp)
    {
        ServicioND(temp);
        temp = temp->sig;
    }
}

/*------------------------------------------------------------------------+
| FUNCION PARA DAR SERVICIO A UN NODO                                     |
+------------------------------------------------------------------------*/

void ServicioND(tNodoD temp)
{
    printf("----\n\n");
    printf("Dato -> %d\n", temp->dato);
    printf("\n----\n");
    getch();
}

/*------------------------------------------------------------------------+
| BUSCAR                                                                  |
+------------------------------------------------------------------------*/

tNodoD BuscarLD(tNodoD listaD, tNodoD ultimo, int valor)
{
    tNodoD  temp = NULL,
            nodo = NULL;

    if(listaD)
    {
        if(valor - listaD->dato < ultimo->dato - valor)
        {
            temp = listaD;
            while(temp && temp->dato <= valor)
            {
                if(temp->dato == valor)
                    return temp;
                temp = temp->sig;
            }
        }
        else
        {
            temp = ultimo;
            while(temp && temp->dato >= valor)
            {
                if(temp->dato == valor)
                    return temp;
                temp = temp->ant;
            }
        }

    }

    return nodo;
}


/*------------------------------------------------------------------------+
| FUNCI�N MENU DESDE DONDE SE MANDA LLAMAR LAS DEMAS FUNCIONES            |
+------------------------------------------------------------------------*/

void menu(void)
{
    tNodoD  nuevo = NULL,
            temp  = NULL,
            listaD = NULL,
            ultimo = NULL;
   	int op, valor;

   	do{
      	system ("cls");
      	printf("\n M  E  N  %c", 233);
	  	printf("\n1.- Agregar");
	  	printf("\n2.- Eliminar");
	  	printf("\n3.- Buscar");
	  	printf("\n4.- Imprimir");
	  	printf("\n5.- Salir\n");
      	op = validaNum(1, 5, "Por favor ingresa una opcion entre", "Opcion invalida");
	  	switch(op)
	   	{
		  	case 1:
		  		nuevo = genDatosD();
		  		printf("Nuevo: %d\n", nuevo->dato);
		  		getch();
		  		AgregarLD(&listaD, &ultimo, &nuevo);
		  	break;
		 	case 2:
		 	    printf("Dime el valor a eliminar\n");
		 	    valor = validaNum(0, 100, "Por favor ingresa una opcion entre", "Opcion invalida");
		 	    temp = BuscarLD(listaD, ultimo, valor);
		 	    if(EliminarLD(&listaD, &ultimo, &temp))
                    printf("Error, valor no encontrado\n");
                else
                    printf("Valor eliminado\n");
                getch();
		  	break;
		  	case 3:
		  	    printf("Dime el valor a buscar");
		 	    valor = validaNum(0, 100, "Por favor ingresa una opcion entre", "Opcion invalida");
		 	    if((temp = BuscarLD(listaD, ultimo, valor)))
                    ServicioND(temp);
                else
                    printf("Error, valor no encontrado\n");
                getch();
            break;
            case 4:
                ImprimirLD(listaD);
            break;
		  	case 5:
                while(listaD)
                {
                    temp = listaD;
                    listaD = temp->sig;
                    free(temp);
                }
            break;
		  	default:
                printf("Opcion invalida");
            break;
	   	}
   }while(op != 5);
}
